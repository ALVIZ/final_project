# final_project

Final project for AOS 573 (Noah Alviz) - Data Analysis of the Jan 1998 Freezing Rain Event in Northeastern U.S. and Southeastern Canada

GOAL/METHODS:
The goal of this project is to analyze the January 1998 freezing rain event that hit the Northeastern U.S. and Southeastern Canada. I mainly want to analyze temperature and precipitation in this region. I will look at temperature horizontally and vertically, and look at precipitation totals over this region. By doing this analysis, I hope to verify the conditions that made freezing rain possible.

DATA:

For vertical profiles of temperature (skew-Ts):
- Unviersity of Wyoming
- http://weather.uwyo.edu/upperair/sounding.html
- For type of plot, select "text: list", then put the appropriate dates/times, and then pick a location to retrieve temperature and dewpoint data
- Focusing on Maniwaki, Québec, Canada since this is the available location in the sounding archives that is closest to the freezing rain event in northeast U.S. and southeast Canada
- Focus on these dates/times: Jan 5th 00Z, Jan 5th 12Z, Jan 6th 00Z, Jan 6th 12Z, Jan 7th 00Z, Jan 7th 12Z (all the year 1998)
- Save this data as a .txt file:
- maniwaki_jan5_00Z.txt
- maniwaki_jan5_12Z.txt
- maniwaki_jan6_00Z.txt
- maniwaki_jan6_12Z.txt
- maniwaki_jan7_00Z.txt
- maniwaki_jan7_12Z.txt
- These text files should have temperature and dewpoint temperature data

For horizontal profiles of temperature (contour plots):
- https://psl.noaa.gov/cgi-bin/db_search/DBListFiles.pl?did=195&tid=96658&vid=13 (link to the data)
- Download the netcdf data called "air.1998.nc"
- Focus on 1000 hpa, 850 hpa, 700 hpa
- Focus on these lat: 35°N to 55°N
- Focus on these lon: -90°W, -60°W

Precipitation data:
- https://downloads.psl.noaa.gov/Datasets/cpc_global_precip/ (link to the data)
- Download the netcdf data called "precip.1998.nc"
- Focus on these dates: Jan 4th-7th
- Focus on these lat: 35°N to 55°N
- Focus on these lon: -90°W, -60°W


